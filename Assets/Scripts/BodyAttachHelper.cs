﻿using System.Collections.Generic;
using Tombstone_Controller;

public static class BodyAttachHelper
{
        public enum BodyAttachResult
        {
            AlreadyAttached,
            NothingToBeFound,
            UndeadPenguin,
            MissingChest,
            AttachedPiece
        }
        
        public static string GetDialogBlockByBodyAttachResult(this BodyAttachResult result)
        {
            switch (result)
            {
                case BodyAttachResult.AlreadyAttached:
                    return "Fail";
                case BodyAttachResult.MissingChest:
                    return "FailChest";
                case BodyAttachResult.NothingToBeFound:
                    return "FoundNothing";
                case BodyAttachResult.UndeadPenguin:
                    return "UndeadPenguin";
                default:
                    return "FoundNothing";
            }
        }


        
        public static BodyAttachResult TryToAttachPiece(TombSearchable piece, List<TombSearchable> bodyPartsCollection)
        {
            //is this an easter egg? PEEEENGUIIIIINNNNN _/(ç.ç)\_
            if (piece == TombSearchable.UndeadPenguin) return BodyAttachResult.UndeadPenguin;
            //does this piece is missing in collection?
            if (bodyPartsCollection.Contains(piece)) return BodyAttachResult.AlreadyAttached;
            //nothing to be found
            if (piece == TombSearchable.NothingToBeFound) return BodyAttachResult.NothingToBeFound;
            if (piece == TombSearchable.SearchedAlready) return BodyAttachResult.NothingToBeFound;
            if (piece == TombSearchable.Uninitialized) return BodyAttachResult.NothingToBeFound;
            //does collection contains torso already?
            //fail torso
            if (piece != TombSearchable.Chest && !bodyPartsCollection.Contains(TombSearchable.Chest)) return BodyAttachResult.MissingChest;
            //piece can be attached
            return BodyAttachResult.AttachedPiece;
        }
}
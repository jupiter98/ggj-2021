﻿using System;
using System.Collections;
using Managers;
using Sound_Controller;
using UnityEngine;
using Random = UnityEngine.Random;


public class LightningEffect : MonoBehaviour
{
    [SerializeField] private Light lightningSource;

    [SerializeField] private float minTime;
    [SerializeField] private float maxTime;
    private void Start()
    {
        StartCoroutine(FlashLight());
    }

    IEnumerator FlashLight()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minTime, maxTime));
            StartCoroutine(Lightning());
        }
    }

    IEnumerator Lightning()
    {
        yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));
        SoundManager.instance.Play(Sound.Names.Thunder);
        lightningSource.enabled = true;
        yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));
        lightningSource.enabled = false;
        yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));
        lightningSource.enabled = true;
        yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));
        lightningSource.enabled = false;
        
        //play thunder sound.
    }
}

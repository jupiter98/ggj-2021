﻿using UnityEngine;

public class InteriorTriggerDetectionScript : MonoBehaviour
{
    [SerializeField] private GameObject gate;
    private CameraScript cameraScript;
    
    void Start()
    {
        cameraScript = Camera.main.GetComponent<CameraScript>();
    }

    void OnTriggerExit(Collider collision)
    {
        cameraScript.ExitCorridor();
        gate.SetActive(true);
    }

    void OnTriggerEnter(Collider collision)
    {
        gate.SetActive(false);
        cameraScript.EnterCorridor();
    }
}

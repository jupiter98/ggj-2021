﻿using System.Collections.Generic;
using System.Linq;
using Tombstone_Controller;
using Random = UnityEngine.Random;

public static class BodyDetachHelper
{
    public static TombSearchable DetachRandomPiece(List<TombSearchable> collection)
    {
        if (collection.Count > 2) //there are both head and chest, and other body pieces. 
        {
            //Remove a random body piece except for head and chest
            var limbs = collection
                .Where(piece => piece != TombSearchable.Head && piece != TombSearchable.Chest)
                .ToList();
            var pieceToRemove = limbs[Random.Range(0, limbs.Count - 1)];
            collection.Remove(pieceToRemove);
            return pieceToRemove;
        }

        //otherwise remove chest first and head at last
        if (collection.Contains(TombSearchable.Chest))
        {
            collection.Remove(TombSearchable.Chest);
            return TombSearchable.Chest;
        }

        collection.Remove(TombSearchable.Head);
        return TombSearchable.Head;
    }
}
﻿using Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningGateScript : MonoBehaviour
{
    [SerializeField]
    private GameObject _leftGate;
    [SerializeField]
    private GameObject _rightGate;

    [SerializeField]
    private Transform _leftPivot;
    [SerializeField]
    private Transform _rightPivot;

    private Vector3 _originalLeftPosition;
    private Vector3 _originalRightPosition;
    private Quaternion _originalLeftRotation;
    private Quaternion _originalRightRotation;

    private Vector3 _openLeftPosition;
    private Vector3 _openRightPosition;
    private Quaternion _openLeftRotation;
    private Quaternion _openRightRotation;
    
    private void Start()
    {
        _originalLeftPosition = _leftGate.transform.position;
        _originalRightPosition = _rightGate.transform.position;
        _originalLeftRotation = _leftGate.transform.rotation;
        _originalRightRotation = _rightGate.transform.rotation;

        _leftGate.transform.RotateAround(_leftPivot.position, Vector3.up, 90f);
        _rightGate.transform.RotateAround(_rightPivot.position, Vector3.up, -90f);

        _openLeftPosition = _leftGate.transform.position;
        _openRightPosition = _rightGate.transform.position;

        _openLeftRotation = _leftGate.transform.rotation;
        _openRightRotation = _rightGate.transform.rotation;

        Close();
    }

    public void Open()
    {
        _leftGate.transform.position = _openLeftPosition;
        _rightGate.transform.position = _openRightPosition;

        _leftGate.transform.rotation = _openLeftRotation;
        _rightGate.transform.rotation = _openRightRotation;
    }

    public void Close()
    {
        _leftGate.transform.position = _originalLeftPosition;
        _rightGate.transform.position = _originalRightPosition;

        _leftGate.transform.rotation = _originalLeftRotation;
        _rightGate.transform.rotation = _originalRightRotation;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (GameManagerScript.Instance?.IsPlayerComplete() == true)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (GameManagerScript.Instance?.IsPlayerComplete() == true)
        {
            Open();
        }
        else
        {
            Close();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Close();
    }
}

﻿using UnityEngine;

namespace Player
{
    public class PlayerConversionScript : MonoBehaviour
    {
        [SerializeField]
        private GameObject _otherPrefab;

        private CameraScript _cameraScript;
        private float _offGroundHeight = 20f;
        private float _magicHeightNumber = 0.125f;

        // Start is called before the first frame update
        private void Start()
        {
            _cameraScript = Camera.main.GetComponent<CameraScript>();
        }

        public void ConvertWithYAngle(float yRotationAngle)
        {
            Quaternion rotation = Quaternion.Euler(Vector3.up * yRotationAngle);
            changePrefabWithRotation(rotation);
        }
        private void changePrefabWithRotation(Quaternion rotation)
        {
            Ray footRay, skyRay;
            RaycastHit hit;
            Vector3 newObjTransform = transform.position;
            footRay = new Ray(transform.position, Vector3.down);
            bool found = Physics.Raycast(footRay, out hit, _offGroundHeight);
            if (found)
            {
                newObjTransform = hit.point;
                //newObjTransform.y += _magicHeightNumber;
            }

            GameObject newPlayer = Instantiate(_otherPrefab, newObjTransform, rotation);

            Destroy(gameObject);
            _cameraScript.updatePlayer(newPlayer);
        }
    }

}

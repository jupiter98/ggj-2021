﻿using System.Collections.Generic;
using System.Linq;
using Tombstone_Controller;
using UnityEngine;

namespace Player
{
    public class PlayerSearchController : MonoBehaviour
    {
        private TombController nearestInteractableTomb = null;
        private readonly List<TombController> _nearbyTombstones = new List<TombController>();

        #region Public
        public void RegisterNearbyTombstone(TombController tomb)
        {
            _nearbyTombstones.Add(tomb);
        }

        public void UnregisterFarawayTombstone(TombController tomb)
        {
            if (!_nearbyTombstones.Contains(tomb)) return;
            tomb.IsEligibleToBeSearched = false;
            _nearbyTombstones.Remove(tomb);
        }

        public TombSearchable SearchTomb()
        {
            if (!nearestInteractableTomb) return TombSearchable.Uninitialized;
            nearestInteractableTomb.IsEligibleToBeSearched = false;
            return nearestInteractableTomb.ShowBodyPartInTomb();
        }

        public void TakeItem()
        {
            nearestInteractableTomb.TakeItem();
        }
        #endregion

        private void Update()
        {
            nearestInteractableTomb = GetNearestTomb();
            if (!nearestInteractableTomb) return;
            nearestInteractableTomb.IsEligibleToBeSearched = true;
            _nearbyTombstones
                .Where(tomb => tomb != nearestInteractableTomb)
                .ToList()
                .ForEach(tomb => tomb.IsEligibleToBeSearched = false);
        }

        private TombController GetNearestTomb()
        {
            return _nearbyTombstones
                .Where(c => c != null && c.HasSomethingToSearch)
                .OrderBy(c => (c.transform.position - transform.position).sqrMagnitude)
                .FirstOrDefault();
        }
    }
}
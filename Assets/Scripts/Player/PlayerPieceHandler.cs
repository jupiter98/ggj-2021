﻿using Managers;
using Tombstone_Controller;
using UnityEngine;

namespace Player
{
    public class PlayerPieceHandler : MonoBehaviour
    {
        [SerializeField] private GameObject leftArm;
        [SerializeField] private GameObject rightArm;
        [SerializeField] private GameObject leftLeg;
        [SerializeField] private GameObject rightLeg;

        private void Update()
        {
            leftArm.SetActive(GameManagerScript.Instance.BodyParts.Contains(TombSearchable.LeftArm));
            rightArm.SetActive(GameManagerScript.Instance.BodyParts.Contains(TombSearchable.RightArm));
            leftLeg.SetActive(GameManagerScript.Instance.BodyParts.Contains(TombSearchable.LeftLeg));
            rightLeg.SetActive(GameManagerScript.Instance.BodyParts.Contains(TombSearchable.RightLeg));
        }
    }
}
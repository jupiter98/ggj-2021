﻿using UnityEngine;

namespace Player
{
    public class HeadMovementScript : MonoBehaviour
    {
        [SerializeField]
        private string _horizontalAxis = "Horizontal";
        [SerializeField]
        private string _verticalAxis = "Vertical";

        private struct HeadMovementStruct
        {
            public bool hasToMove;
            public Vector3 direction;
        }

        private HeadMovementStruct _movement;

        [SerializeField]
        private float forceMagnitude = 1f;

        // Update is called once per frame
        private void Update()
        {
            float horizontal = Input.GetAxis(_horizontalAxis);
            float vertical = Input.GetAxis(_verticalAxis);

            _movement.hasToMove = false;

            Vector3 direction = Vector3.zero;
            if (vertical > 0f)
            {
                direction += Vector3.forward;
                _movement.hasToMove = true;
            }
            else if(vertical < 0f)
            {
                direction -= Vector3.forward;
                _movement.hasToMove = true;
            }

            if(horizontal > 0f)
            {
                direction += Vector3.right;
                _movement.hasToMove = true;
            }
            else if (horizontal < 0f)
            {
                direction -= Vector3.right;
                _movement.hasToMove = true;
            }
            direction = direction.normalized;
            _movement.direction = direction;

        }

        private void FixedUpdate()
        {
            if (_movement.hasToMove)
            {
                this.gameObject.GetComponent<Rigidbody>().AddForce(_movement.direction * forceMagnitude, ForceMode.Force);
            }
        }
    }
}

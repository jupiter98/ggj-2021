﻿using Managers;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Animator))]
    public class PlayerMovement : MonoBehaviour
    {
        public enum PlayerMoveType
        {
            NoLeg,
            OneLeg,
            TwoLeg
        }
        
        private Animator animator;

        [SerializeField]
        private CharacterController controller;
        public float speed = 6f;

        private PlayerMoveType _moveType = PlayerMoveType.NoLeg;
        
        public float turnSmoothTime = 0.1f;
        private float _turnSmoothVelocity;

        private void Start()
        {
            animator = GetComponent<Animator>();
        }

        private void Update()
        {
            UpdateMoveType();

            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");

            Vector3 direction = new Vector3(horizontal, 0, vertical).normalized;

            bool isMoving = direction.magnitude >= 0.1f;
            if (isMoving)
            {
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref _turnSmoothVelocity,
                    turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                Vector3 moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                controller.Move(moveDirection.normalized * (speed * Time.deltaTime));
            }

            animator.SetBool("walk", isMoving);
        }
        
        private void UpdateMoveType()
        {
            PlayerMoveType prevMoveType = _moveType;

            bool hasALeg = GameManagerScript.Instance.HasAtLeastALeg();
            bool hasBothLegs = GameManagerScript.Instance.HasBothLegs();

            if (!hasALeg)
            {
                _moveType = PlayerMoveType.NoLeg;
            }

            if (!hasBothLegs && hasALeg)
            {
                _moveType = PlayerMoveType.OneLeg;
            }

            if (hasBothLegs)
            {
                _moveType = PlayerMoveType.TwoLeg;
            }

            animator.SetBool(GetMoveAnimationByMoveType(prevMoveType), false);
            animator.SetBool(GetMoveAnimationByMoveType(_moveType), true);
        }

        private string GetMoveAnimationByMoveType(PlayerMoveType moveType)
        {
            switch (moveType)
            {
                case PlayerMoveType.OneLeg: return "oneLeg";
                case PlayerMoveType.TwoLeg: return "twoLegs";
                default: return "noLeg";
            }
        }
    }
}
﻿using System;
using Managers;
using Tombstone_Controller;
using UnityEngine;

namespace Player
{
    
    [RequireComponent(typeof(PlayerSearchController))]
    [RequireComponent(typeof(PlayerConversionScript))]
    public class MainPlayerController : MonoBehaviour
    {

        private PlayerSearchController _searchController;
        private PlayerConversionScript conversionController;

        #region Lifecycle
        private void Start()
        {
            _searchController = GetComponent<PlayerSearchController>();
            conversionController = GetComponent<PlayerConversionScript>();
        }

        private void Update()
        {
            if (Input.GetButtonDown("Search Tomb"))
            {
                Search();
            }
        }
        #endregion

        public void DropABodyPiece()
        {
            if(GameManagerScript.Instance.DropABodyPiece() == TombSearchable.Chest)
                UpdatePlayerShape();
        }
        private void UpdatePlayerShape()
        {
            conversionController.ConvertWithYAngle(180f);    
        }
        
        private void Search()
        {
            var searchedPiece = _searchController.SearchTomb();
            if(TombSearchable.Uninitialized == searchedPiece) return;
 
            var bodyAttachResult = BodyAttachHelper.TryToAttachPiece(searchedPiece, GameManagerScript.Instance.BodyParts);
            if (bodyAttachResult == BodyAttachHelper.BodyAttachResult.AttachedPiece ||
                bodyAttachResult == BodyAttachHelper.BodyAttachResult.NothingToBeFound ||
                bodyAttachResult == BodyAttachHelper.BodyAttachResult.UndeadPenguin)
            {
                _searchController.TakeItem();
            }
            if(bodyAttachResult == BodyAttachHelper.BodyAttachResult.AttachedPiece)
            {
                if(GameManagerScript.Instance.IsPlayerOnlyHead() )
                    UpdatePlayerShape();
                GameManagerScript.Instance.TakeItemFromGrave(searchedPiece);
            }
            String block = bodyAttachResult == BodyAttachHelper.BodyAttachResult.AttachedPiece
                ? searchedPiece.GetDialogBlockByBodyPiece()
                : bodyAttachResult.GetDialogBlockByBodyAttachResult();
            UiManager.Instance?.ShowDialog(block);
        }
    }
}
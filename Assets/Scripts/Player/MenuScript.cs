﻿using UnityEngine;

namespace Player
{
    public class MenuScript : MonoBehaviour
    {
        [SerializeField]
        private string mainLevel = "MainScene";

        [SerializeField]
        private string creditsScene = "CreditsScene";

        public void StartGame()
        {
            Application.LoadLevel(mainLevel);
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        public void Credits()
        {
            Application.LoadLevel(creditsScene);
        }

    }
}

﻿using Managers;
using UnityEngine;

namespace Player
{
    public class ExitTriggerDetectionScript : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            GameManagerScript.Instance.TryToExit();
        }
    }
}

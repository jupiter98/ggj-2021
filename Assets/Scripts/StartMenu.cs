﻿using System;
using System.Collections;
using System.Collections.Generic;
using Managers;
using Sound_Controller;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour
{

    [SerializeField] private GameObject creditsUI;
    [SerializeField] private GameObject startUI;
    [SerializeField] private Image fadeImage;

    private void Start()
    {
        fadeImage.CrossFadeAlpha(0,0,true);
    }

    public void StartGame()
    {
        StartCoroutine(LoadGame());
    }

    private IEnumerator LoadGame()
    {
        fadeImage.CrossFadeAlpha(1,2,true);
        yield return new WaitForSeconds(2);
        SoundManager.instance?.EnableAmbientSounds();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        
    }

    public void QuitGame()
    {
        #if  UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void Credits()
    {
        creditsUI.SetActive(true);
        startUI.SetActive(false);
        SoundManager.instance?.Play(Sound.Names.Shouts);
    }
    
    public void QuitCredits()
    {
        creditsUI.SetActive(false);
        startUI.SetActive(true);
    }

}

﻿using Fungus;
using Managers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class PauseMenu : MonoBehaviour
    {
        private static bool _isPaused = false;

        public GameObject pauseUI;
        public GameObject fungusSytem;
        
        private void Update()
        {
            if (Input.GetButtonDown("Cancel"))
            {
                if (_isPaused)
                {
                    ResumeGame();
                }
                else
                {
                    Pause();
                }
            }
        }

        public void Pause()
        {

            pauseUI.SetActive(true);
            Time.timeScale = 0f;
            _isPaused = true;
            fungusSytem.GetComponent<AudioSource>().Stop();
        }

        public void ResumeGame()
        {
            pauseUI.SetActive(false);
            Time.timeScale = 1f;
            _isPaused = false;
            fungusSytem.GetComponent<AudioSource>().Play();
        }

        public void ReloadScene()
        {
            var scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.buildIndex);
            Time.timeScale = 1;
            _isPaused = false;
        }
        
        public void QuitToMainMenu()
        {
            var scene = SceneManager.GetActiveScene();
            SoundManager.instance?.DisableAmbientSounds();
            SceneManager.LoadScene(scene.buildIndex - 1);
            Time.timeScale = 1;
            _isPaused = false;
        }
    }
}

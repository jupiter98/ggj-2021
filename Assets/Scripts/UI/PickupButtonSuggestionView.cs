﻿using Tombstone_Controller;
using UnityEngine;

namespace UI
{
    public class PickupButtonSuggestionView : MonoBehaviour
    {
        [SerializeField] private TombController mainTombController;
        [SerializeField] private Sprite sprite;
        private Camera _mainCamera;
        private SpriteRenderer _render;

        #region Lifecycle
        private void Start()
        {
            _render = GetComponent<SpriteRenderer>();
            mainTombController.onPlayerNearbyTombstoneChanged += OnPlayerNearby;
            _mainCamera = Camera.main;
        }

        private void LateUpdate()
        {
            transform.LookAt(_mainCamera.transform);
        }

        private void OnDestroy()
        {
            if (mainTombController != null)
                mainTombController.onPlayerNearbyTombstoneChanged -= OnPlayerNearby;
        }
        #endregion

        private void OnPlayerNearby(bool isPlayerNearby)
        {
            _render.sprite = isPlayerNearby ? sprite : null;
        }
    }
}

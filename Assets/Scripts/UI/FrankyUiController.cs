﻿using System.Collections;
using System.Collections.Generic;
using Tombstone_Controller;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class FrankyUiController : MonoBehaviour
    {
        [Header("Color config")] 
        [SerializeField]private Color defaultColor;
        [SerializeField]private Color activeColor;
        [SerializeField] private Color damageColor;

        [Header("Body parts")]
        [SerializeField] private Image head;
        [SerializeField] private Image chest;
        [SerializeField] private Image leftArm;
        [SerializeField] private Image rightArm;
        [SerializeField] private Image leftLeg;
        [SerializeField] private Image rightLeg;

        [Header("Effects")]
        [SerializeField] private float damageEffectDuration = 1f;

        private readonly Dictionary<Image, Coroutine> imageProcesses = new Dictionary<Image, Coroutine>();

        private void Start()
        {
            imageProcesses.Clear();
            imageProcesses.Add(head, null);
            imageProcesses.Add(chest, null);
            imageProcesses.Add(leftArm, null);
            imageProcesses.Add(rightArm, null);
            imageProcesses.Add(leftLeg, null);
            imageProcesses.Add(rightLeg, null);
        }

        #region Public
        public void SetPieceActive(TombSearchable piece, bool isActive)
        {
            var image = ConvertFromTombSearchableToUiPiece(piece);
            if (!image) return;
            if (imageProcesses.ContainsKey(image) && imageProcesses[image] != null)
            {
                StopCoroutine(imageProcesses[image]);
                imageProcesses[image] = null;
                image.color = defaultColor;
            }
            image.color = isActive ? activeColor : defaultColor;
        }


        public void AddPiece(TombSearchable piece)
        {
            var image = ConvertFromTombSearchableToUiPiece(piece);
            if (!image) return;
            if (imageProcesses.ContainsKey(image) && imageProcesses[image] != null)
            {
                StopCoroutine(imageProcesses[image]);
                imageProcesses[image] = null;
                image.color = defaultColor;
            }
            image.color = activeColor;
        }

        public void RemovePiece(TombSearchable piece)
        {
            var image = ConvertFromTombSearchableToUiPiece(piece);
            if (!image) return;
            if (imageProcesses.ContainsKey(image) && imageProcesses[image] != null)
            {
                StopCoroutine(imageProcesses[image]);
                imageProcesses[image] = null;
                image.color = defaultColor;
            }
            image.color = damageColor;
            imageProcesses[image] = StartCoroutine(damageTilt(image, damageColor, defaultColor, damageEffectDuration));
        }
        #endregion

        #region Private
        private IEnumerator damageTilt(Image image, Color fromColor, Color toColor, float duration)
        {
            for (float t = 0f; t < damageEffectDuration; t += Time.deltaTime)
            {
                image.color = Color.Lerp(fromColor, toColor, t / duration);
                yield return 0;
            }
            image.color = toColor;
        }
        
        private Image ConvertFromTombSearchableToUiPiece(TombSearchable piece)
        {
            switch (piece)
            {
                case TombSearchable.Head: return head;
                case TombSearchable.Chest: return chest;
                case TombSearchable.LeftArm: return leftArm;
                case TombSearchable.RightArm: return rightArm;
                case TombSearchable.LeftLeg: return leftLeg;
                case TombSearchable.RightLeg: return rightLeg;
                default: return null;
            }
        }
        #endregion
    }
}

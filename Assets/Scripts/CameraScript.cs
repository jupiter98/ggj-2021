﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField]
    private string _playerTag = "Player";

    private GameObject _player;

    private Vector3 _offset;
    private Quaternion _rotation;

    [SerializeField]
    private Transform _closeTransformPosition;
    [SerializeField]
    private Transform _farTransformPosition;

    private Vector3 _closeOffset;
    private Vector3 _farOffset;
    private Vector3 _playerOffset;

    private Quaternion _closeRotation;
    private Quaternion _farRotation;

    private Coroutine _offsetCoroutine;

    public void updatePlayer(GameObject player)
    {
        this._player = player;
    }

    private void Start() //sets a distance offset that the camera need to maintain during gameplay.
    {
        _player = GameObject.FindGameObjectWithTag(_playerTag);

        _playerOffset = _player.transform.position;

        _farOffset = _farTransformPosition.position;
        _closeOffset = _closeTransformPosition.position;
        _offset = _closeOffset;


        _closeRotation = _closeTransformPosition.rotation;
        _farRotation = _farTransformPosition.rotation;

        _rotation = _closeRotation;
    }

    private void LateUpdate() // updates the position of the camera relative to the player.
    {
        transform.position = _player.transform.position + _offset - _playerOffset;
        transform.rotation = _rotation;
    }

    public void ExitCorridor()
    {
            if (_offsetCoroutine != null)
            {
                StopCoroutine(_offsetCoroutine);
            }
            _offsetCoroutine = StartCoroutine(LerpTo(_farOffset, _farRotation, 1f));
    }


    public void EnterCorridor()
    {
            if (_offsetCoroutine != null)
            {
                StopCoroutine(_offsetCoroutine);
            }
            _offsetCoroutine = StartCoroutine(LerpTo(_closeOffset, _closeRotation, 1f));
    }

    private IEnumerator LerpTo(Vector3 pos2, Quaternion rotation2, float duration)
    {
        for (float t = 0f; t < duration; t += Time.deltaTime)
        {
            _offset = Vector3.Slerp(_offset, pos2, t / duration);
            _rotation = Quaternion.Slerp(_rotation, rotation2, t / duration);
            yield return 0;
        }
        _offset = pos2;
        _rotation = rotation2;
    }
}

﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Ai_Controllers
{
    public abstract class BaseIaState
    {
        public abstract string Animation { get; }
        public static bool isChasing = false;
        
        public BaseIaState Execute(BaseIaState currentState, EnemyIA ia)
        {
            Execute_Impl(ia);
            BaseIaState newState = GetNextState(ia);
            if(currentState != newState)
                newState.OnEnterStatus(ia);
            return newState;
        }

        protected abstract void Execute_Impl(EnemyIA ia);
        protected abstract BaseIaState GetNextState(EnemyIA ia);
        
        protected virtual void OnEnterStatus(EnemyIA ia) {}
    }

    public class IdleState : BaseIaState
    {
        public override string Animation => "Search";

        public static readonly BaseIaState Instance = new IdleState();
        protected override void Execute_Impl(EnemyIA ia) { }

        protected override BaseIaState GetNextState(EnemyIA ia)
        {
            return PatrollingState.Instance;
        }

        protected override void OnEnterStatus(EnemyIA ia)
        {
            ia.StartTriggerAnimation(Animation);
        }
    }

    public class PatrollingState : BaseIaState
    {
        public override string Animation => "Patroll";

        public static readonly BaseIaState Instance = new PatrollingState();

        private bool stopPatrolling = false;
        private int _currentWaypoint = 0;
        private int _patrollingCounter = -1;

        protected override void OnEnterStatus(EnemyIA ia)
        {
            ia.StartTriggerAnimation(Animation);
        }

        protected override void Execute_Impl(EnemyIA ia)
        {
            stopPatrolling = false;
            UpdateWaypoint(ia);
            ia.Patroll(_currentWaypoint);
        }
        
        protected override BaseIaState GetNextState(EnemyIA ia)
        {
            if(isChasing) return ChasingState.Instance;
            return stopPatrolling ? SearchingState.Instance : Instance;
        }

        private void UpdateWaypoint(EnemyIA ia)
        {
            if (_patrollingCounter <= 0)
            {
                if(_patrollingCounter >= 0)
                    stopPatrolling = true;
                _patrollingCounter = Random.Range(1, ia.GetWaypointsLength());
                return;
            }
            
            if (!ia.IsIaArrivedAtDestination(_currentWaypoint)) return;

            if (_currentWaypoint == ia.GetWaypointsLength() - 1) 
                _currentWaypoint = 0;
            else _currentWaypoint++;
            
            _patrollingCounter--;
        }
    }
    
    public class SearchingState : BaseIaState
    {
        public override string Animation => "Search";

        private float timer = 3f;
        private bool oneTimeSearching = true;
        private bool isSearching = false;
        
        public static readonly BaseIaState Instance = new SearchingState();

        protected override void OnEnterStatus(EnemyIA ia)
        {
            ia.StartTriggerAnimation(Animation);
        }

        protected override void Execute_Impl(EnemyIA ia)
        {
            if (oneTimeSearching && !isSearching)
            {
                oneTimeSearching = false;
                ia.StartSearch(timer, OnSearchStateChanged);
            }
        }

        protected override BaseIaState GetNextState(EnemyIA ia)
        {
            if (isChasing)
            {
                isSearching = false;
                oneTimeSearching = true;
                return ChasingState.Instance;
            }

            if (!isSearching)
            {
                oneTimeSearching = true;
                return PatrollingState.Instance;
            }
            
            return Instance;
        }

        private void OnSearchStateChanged(bool isActive)
        {
            isSearching = isActive;
        }
        
    }
    
    public class ChasingState : BaseIaState
    {
        public override string Animation => "Chase";

        public static readonly BaseIaState Instance = new ChasingState();

        protected override void OnEnterStatus(EnemyIA ia)
        {
            ia.StartTriggerAnimation(Animation);
        }

        protected override void Execute_Impl(EnemyIA ia)
        {
            ia.Chase();
        }

        protected override BaseIaState GetNextState(EnemyIA ia)
        {
            if(!isChasing) return SearchingState.Instance;
            if(ia.IsTargetInRange()) return AttackingState.Instance;
            return Instance;
        }
    }
    
    public class AttackingState : BaseIaState
    {
        public override string Animation => "Attack";

        public static readonly BaseIaState Instance = new AttackingState();
        protected override void Execute_Impl(EnemyIA ia)
        {
            ia.TryToAttack();
        }

        protected override BaseIaState GetNextState(EnemyIA ia)
        {
            if(!ia.IsTargetInRange())
            {
                ia.ResetCooldown();
                return ChasingState.Instance;
            }
            return Instance;
        }
    }
}


﻿using System;
using System.Collections;
using Managers;
using Player;
using Sound_Controller;
using UnityEngine;
using UnityEngine.AI;

namespace Ai_Controllers
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(Animator))]
    public class EnemyIA : MonoBehaviour
    {
        private NavMeshAgent agent;
        private Animator animator;
        
        [Header("Patrolling config")]
        [SerializeField] private Transform[] waypoints;
        [SerializeField] private float distanceOffsetFromWaypoint = 1f;
        [SerializeField] private float patrollingSpeed = 1f;
        
        [Header("Chasing config")]
        [SerializeField] private float chasingSpeed = 1f;
        
        [Header("Attack config")]
        [SerializeField] private float attackRate = 2f;
        private float cooldown = 0f;
        private bool isTargetInRange = false;
        
        #region Fov Variables

        public float viewRadius;
        [Range(0, 360)] public float viewAngle;

        public LayerMask targetMask;
        public LayerMask obstacleMask;
        #endregion
        
        private MainPlayerController target = null;

        private BaseIaState _state = IdleState.Instance;

        #region Lifecycle
        private void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            animator = GetComponent<Animator>();
            
            FindTarget();
        }

        private void Update()
        {
            _state = _state.Execute(_state, this);
            Debug.Log($"AI STATE {_state}");
        }
        
        #endregion
        
        #region  Animator

        public void StartTriggerAnimation(string anim)
        {
            animator.SetTrigger(anim);
        }

        #endregion
        
        #region Agent

        public void Patroll(int nextWaypoint)
        {
            agent.speed = patrollingSpeed;
            agent.SetDestination(waypoints[nextWaypoint].transform.position);
        }

        public void Chase()
        {
            var targetPosition = target == null ? Vector3.zero : target.transform.position;
            if (targetPosition != Vector3.zero)
            {
                agent.speed = chasingSpeed;
                agent.isStopped = false;
                agent.SetDestination(targetPosition);
            }
            else BaseIaState.isChasing = false;
        }

        #endregion

        #region Patrolling

        private Vector3 GetNewWaypointPosition(int waypointIndex)
        {
            return waypoints[waypointIndex].transform.position;
        }

        public int GetWaypointsLength()
        {
            return waypoints.Length;
        }

        public bool IsIaArrivedAtDestination(int waypointIndex)
        {
            return Vector3.Distance(transform.position, GetNewWaypointPosition(waypointIndex)) <= distanceOffsetFromWaypoint;
        }

        #endregion
        
        #region Searching

        public void StartSearch(float timer, Action<bool> onSearch)
        {
            StartCoroutine(Search(timer, onSearch));
        }

        private IEnumerator Search(float timer, Action<bool> onSearch)
        {
            onSearch?.Invoke(true);

            agent.isStopped = true;
            while (timer > 0)
            {
                timer -= Time.deltaTime;
                yield return null;
            }

            agent.isStopped = false;
            onSearch?.Invoke(false);
        }

        #endregion

        #region Chasing

        public bool IsTargetInRange()
        {
            return isTargetInRange;
        }

        public void InterceptionChangeDetection(bool isInRange) //anchor to know when target is in range
        {
            isTargetInRange = isInRange;
        } 
        
        private void FindTarget()
        {
            StartCoroutine(nameof(FindTargetsWithDelay), .2f);
        }

        IEnumerator FindTargetsWithDelay(float delay)
        {
            while (true)
            {
                yield return new WaitForSeconds(delay);
                FindVisibleTargets();
            }
        }

        private void FindVisibleTargets()
        {
            Collider[] targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);
            
            if (targetsInViewRadius.Length > 0)
            {
                CancelInvoke(nameof(StopChasing));
                Transform foundTarget = targetsInViewRadius[0].transform;
                Vector3 dirToTarget = (foundTarget.position - transform.position).normalized;
                bool isTargetInSight = Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2;
                if (isTargetInSight)
                {
                    float dstToTarget = Vector3.Distance(transform.position, foundTarget.position);
                    bool detectedObstacle = Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstacleMask);
                    if (!detectedObstacle && target == null)
                    {
                        target = foundTarget.GetComponent<MainPlayerController>();
                        BaseIaState.isChasing = true;
                        isTargetInRange = false;
                        PlayGuardianAlert();
                        return;
                    }
                }
            }
            if(!IsInvoking(nameof(StopChasing)))
                Invoke(nameof(StopChasing), 2f);
        }

        private void StopChasing()
        {
            BaseIaState.isChasing = false;
            target = null;
            isTargetInRange = false;
        }
        
        #endregion

        #region Attacking

        public void TryToAttack()
        {
            if (target == null)
            {
                return;
            }
            //stop chasing to launch an attack
            agent.isStopped = true;
            agent.velocity = Vector3.zero;
            
            cooldown -= Time.deltaTime;
            if (cooldown <= 0)
            {
                cooldown = attackRate;
                Attack();
            }
        }

        public void ResetCooldown()
        {
            cooldown = 0;
        }

        private void Attack()
        {
            animator.SetTrigger(_state.Animation);
        }

        private void OnAttackEnd() //should be called by the attack animation end event
        {
            if (target != null && IsTargetInRange())
            {
                animator.SetTrigger("AttackIdle");
                target.DropABodyPiece();
                return;
            }
            animator.SetTrigger(_state.Animation);

        }

        private void PlayScytheSound() //should be used to play attack sound
        {
            SoundManager.instance?.Play(Sound.Names.BladeSwing);
        }
        
        private void PlayGuardianAlert() //should be used to play guardian alert sound
        {
            SoundManager.instance?.Play(Sound.Names.AlertSound);
        }

        #endregion
    }
}
﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Ai_Controllers
{
    [Serializable]
    public class InterceptionStateEvent : UnityEvent<bool> { }
    public class AiInterceptorController : MonoBehaviour
    {
        public InterceptionStateEvent onInterceptionStateChange; 
        private void OnTriggerEnter(Collider other)
        {
            onInterceptionStateChange?.Invoke(true);
        }

        private void OnTriggerStay(Collider other)
        {
            onInterceptionStateChange?.Invoke(true);
        }

        private void OnTriggerExit(Collider other)
        {
            onInterceptionStateChange?.Invoke(false);
        }
        
    }
}
﻿using UnityEngine;

namespace Sound_Controller
{
    [System.Serializable]
    public class Sound {

        [HideInInspector]
        public AudioSource source;
        public enum Names
        {
            WindHowl,
            Rain,
            Horse,
            BladeSwing,
            AlertSound,
            BgScreams,
            Thunder,
            Shouts
        }
        public Names name = Names.WindHowl;
        
        public AudioClip clip;

        [Range(0f, 1f)]
        public float volume = .75f;
        [Range(0f, 1f)]
        public float volumeVariance = .1f;

        [Range(.1f, 3f)]
        public float pitch = 1f;
        [Range(0f, 1f)]
        public float pitchVariance = .1f;

        public bool loop = false;

        public bool isRandom = false;
        
        public bool canBeOverridden = false;
    }
}

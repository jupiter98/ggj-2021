﻿using System;
using Managers;
using Player;
using UnityEngine;

namespace Tombstone_Controller
{
    public class TombController : MonoBehaviour
    {
        public event Action<bool> onPlayerNearbyTombstoneChanged;

        [SerializeField] private GameObject tomb;
        /**
         * Franky body part hidden inside
         * If not equal to null it means this tomb can be searched
         */
        [SerializeField] private TombSearchable tombSearchableHiddenInside = TombSearchable.Uninitialized;

        public bool IsEligibleToBeSearched
        {
            get => _isEligibleToBeSearched;
            set => _isEligibleToBeSearched = HasSomethingToSearch && value;
        }
        public bool HasSomethingToSearch => TombSearchable.Uninitialized != tombSearchableHiddenInside;
        public bool HasNothingToSearch => TombSearchable.Uninitialized == tombSearchableHiddenInside;

        [Header("Detection range")]
        [SerializeField] [Range(0, 20)] private float detectionRange = 0.1f;
        [SerializeField] private LayerMask layer;
        [SerializeField] private Animator graveAnimator;

        private PlayerSearchController _player = null;
        private bool _isEligibleToBeSearched = false;
        [SerializeField] private ParticleSystem _fleshParticles;
        [SerializeField]
        private bool _alreadyDigged;


        #region Public
        public void HideBodyPartInTomb(TombSearchable tombSearchable)
        {
            tombSearchableHiddenInside = tombSearchable;
        }

        public TombSearchable ShowBodyPartInTomb()
        {
            return tombSearchableHiddenInside;
        }

        public void TakeItem()
        {
            _fleshParticles.Play();
            tombSearchableHiddenInside = TombSearchable.SearchedAlready;
            _alreadyDigged = true;
            graveAnimator.Play("Grave_Animation");
        }
        #endregion

        #region Lifecycle

        private void FixedUpdate()
        {
            onPlayerNearbyTombstoneChanged?.Invoke(_player && IsEligibleToBeSearched);
        }

        private void Update()
        {
            SearchForPlayer();
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, detectionRange);
        }
#endif
        #endregion

        #region Private
        private void SearchForPlayer()
        {
            Collider[] targets = new Collider[1];
            Physics.OverlapSphereNonAlloc(transform.position, detectionRange, targets, layer);
            bool isPlayerNearby = _player != null;
            bool hasPlayerBeenDetected = targets[0] != null;
            if (!isPlayerNearby && hasPlayerBeenDetected && !_alreadyDigged)
            {
                _player = targets[0].gameObject.GetComponent<PlayerSearchController>();
                _player.RegisterNearbyTombstone(this);
            }
            else if (isPlayerNearby && (!hasPlayerBeenDetected || _alreadyDigged))
            {
                _player.UnregisterFarawayTombstone(this);
                _player = null;
            }
        }

        #endregion
    }

}

﻿using UI;
using UnityEngine;
using Utils;

namespace Tombstone_Controller
{
    public class TombOutlineController : MonoBehaviour
    {
        [SerializeField] private TombController _controller;
        [SerializeField] private Outline _outlineController;
        private void Start()
        {
            _controller.onPlayerNearbyTombstoneChanged += OnPlayerNearbyTombstoneChanged;
        }

        private void OnDestroy()
        {
            if(_controller != null)
                _controller.onPlayerNearbyTombstoneChanged -= OnPlayerNearbyTombstoneChanged;
        }

        private void OnPlayerNearbyTombstoneChanged(bool isPlayerNearby)
        {
            _outlineController.enabled = isPlayerNearby;
        }
    }
}
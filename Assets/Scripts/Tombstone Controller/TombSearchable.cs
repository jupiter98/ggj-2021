﻿using System;

namespace Tombstone_Controller
{
    [Serializable]
    public enum TombSearchable
    {
        Head,
        LeftArm,
        RightArm,
        LeftLeg,
        RightLeg,
        Chest,
        NothingToBeFound,
        UndeadPenguin,
        SearchedAlready,
        Uninitialized
    }

    public static class TombSearchableExtension
    {
        public static string GetDialogBlockByBodyPiece(this TombSearchable piece)
        {
            switch (piece)
            {
                case TombSearchable.RightLeg:
                case TombSearchable.LeftLeg:
                    return "Leg";
                case TombSearchable.Chest:
                    return "Chest";
                case TombSearchable.RightArm:
                case TombSearchable.LeftArm:
                    return "Arm";
                default:
                    return "FoundNothing";
            }
        }
    }
}
﻿using System;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    
    [SerializeField] private GameObject player;
    private Vector3 _offset;

    private Vector3 _startOffset;

    private void Start () //sets a distance offset that the camera need to maintain during gameplay.
    {
        _offset = transform.position - player.transform.position;
    }

    private void LateUpdate () // updates the position of the camera relative to the player.
    {
        transform.position = player.transform.position + _offset;
    }

}

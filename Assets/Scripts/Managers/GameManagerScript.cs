﻿using System.Collections;
using System.Collections.Generic;
using Tombstone_Controller;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Managers
{

    public class GameManagerScript : MonoBehaviour
    {
        public static GameManagerScript Instance = null;

        [SerializeField]
        private GameObject fungusSystem;
        [SerializeField]
        private Image _foregroundImage;
        [SerializeField]
        private float _reloadGameDelay = 2f;
        [SerializeField]
        private float _blackScreenAfter = 0.5f;
        [SerializeField]
        private float _blackScreenDelta = 1f;

        private const int PlayerComplete = 6;
        private const int PlayerDead = 0;
        private const int PlayerHasOnlyHead = 1;

        [Header("Tombstone Config")]
        #region Tombston class members
        [SerializeField]
        private string _tombstoneTag = "Tombstone";

        [SerializeField]
        private int _numLeftArms = 1;
        [SerializeField]
        private int _numRightArms = 1;
        [SerializeField]
        private int _numLeftLegs = 1;
        [SerializeField]
        private int _numRightLegs = 1;
        [SerializeField]
        private int _numChests = 1;

        private int _numEmpty;

        private int[] _numPieces = new int[PlayerComplete];
        private int[] _counters = new int[PlayerComplete];

        private List<TombController> _tombControllers = new List<TombController>();
        #endregion

        private readonly List<TombSearchable> _playerBodyParts = new List<TombSearchable>() { TombSearchable.Head };
        public List<TombSearchable> BodyParts => _playerBodyParts;

        #region Public

        public bool HasAtLeastALeg()
        {
            return BodyParts.Contains(TombSearchable.RightLeg) || BodyParts.Contains(TombSearchable.LeftLeg);
        }
    
        public bool HasBothLegs()
        {
            return BodyParts.Contains(TombSearchable.RightLeg) && BodyParts.Contains(TombSearchable.LeftLeg);
        }
        public TombSearchable DropABodyPiece()
        {
            var removedPiece = BodyDetachHelper.DetachRandomPiece(BodyParts);
            UiManager.Instance.RemovePiece(removedPiece);
            if (IsPlayerDead())
            {
                gameOver(false);
            }

            return removedPiece;
        }
        
        public bool IsPlayerOnlyHead()
        {
            return _playerBodyParts.Count == PlayerHasOnlyHead;
        }

        public void TryToExit()
        {
            if (IsPlayerComplete())
            {
                gameOver(true);
            }
            else
            {
                UiManager.Instance?.ShowDialog("FailExit");
            }
        }

        public void TakeItemFromGrave(TombSearchable content)
        {
            //add piece to collection
            BodyParts.Add(content);
            UiManager.Instance?.AddPiece(content);

            int index = tombSearchableToInt(content);
            if (index >= 0)
            {
                _counters[index]--;
            }
            bool otherTombs = false;
            for (int i = 0; i < _counters.Length && !otherTombs; i++)
            {
                otherTombs = _counters[i] > 0;
            }
            if (!otherTombs && !IsPlayerComplete())
            {
                gameOver(false);
            }
        }

        #endregion

        #region Lifecycle

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        void Start()
        {
            foreach (GameObject go in GameObject.FindGameObjectsWithTag(_tombstoneTag))
            {
                _tombControllers.Add(go.GetComponent<TombController>());
            }
            _numEmpty = _tombControllers.FindAll(arg => arg.HasNothingToSearch).Count
                - _numLeftArms - _numRightArms - _numLeftLegs - _numRightLegs - _numChests;
            initGraves();
            UiManager.Instance?.InitBodyUi(_playerBodyParts);
        }
        #endregion

        #region Tombstone initialization
        private TombSearchable intToTombSearchable(int index)
        {
            switch (index)
            {
                case 0:
                    return TombSearchable.LeftArm;
                case 1:
                    return TombSearchable.RightArm;
                case 2:
                    return TombSearchable.LeftLeg;
                case 3:
                    return TombSearchable.RightLeg;
                case 4:
                    return TombSearchable.Chest;
                case 5:
                    return TombSearchable.NothingToBeFound;
                default:
                    return TombSearchable.Uninitialized;
            }
        }

        private int tombSearchableToInt(TombSearchable content)
        {
            switch (content)
            {
                case TombSearchable.LeftArm:
                    return 0;
                case TombSearchable.RightArm:
                    return 1;
                case TombSearchable.LeftLeg:
                    return 2;
                case TombSearchable.RightLeg:
                    return 3;
                case TombSearchable.Chest:
                    return 4;
                case TombSearchable.NothingToBeFound:
                    return 5;
                default:
                    return -1;
            }
        }

        private void initGraves()
        {
            _numPieces[0] = _numLeftArms;
            _numPieces[1] = _numRightArms;
            _numPieces[2] = _numLeftLegs;
            _numPieces[3] = _numRightLegs;
            _numPieces[4] = _numChests;
            _numPieces[5] = _numEmpty;

            _counters[0] = 0; // _numLeftArms;
            _counters[1] = 0; // _numRightArms;
            _counters[2] = 0; // _numLeftLegs;
            _counters[3] = 0; // _numRightLegs;
            _counters[4] = 0; // _numChests;
            _counters[5] = 0; // _numEmpty;

            System.Random random = new System.Random();

            foreach (TombController tc in _tombControllers)
            {
                if (tc.HasNothingToSearch)
                {
                    bool found = false;
                    while (!found)
                    {
                        int caseNum = random.Next(0, 6);
                        if ((_numPieces[caseNum] - _counters[caseNum]) > 0)
                        {
                            found = true;
                            tc.HideBodyPartInTomb(intToTombSearchable(caseNum));
                            _counters[caseNum]++;
                        }
                    }
                }
                else
                {
                    switch (tc.ShowBodyPartInTomb())
                    {
                        //case TombSearchable.Head:
                        //    break;
                        case TombSearchable.LeftArm:
                            _counters[0]++;
                            break;
                        case TombSearchable.RightArm:
                            _counters[1]++;
                            break;
                        case TombSearchable.LeftLeg:
                            _counters[2]++;
                            break;
                        case TombSearchable.RightLeg:
                            _counters[3]++;
                            break;
                        case TombSearchable.Chest:
                            _counters[4]++;
                            break;
                        case TombSearchable.NothingToBeFound:
                            _counters[5]++;
                            break;
                        case TombSearchable.UndeadPenguin:
                            break;
                        case TombSearchable.SearchedAlready:
                            break;
                        case TombSearchable.Uninitialized:
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        #endregion

        #region Private

        public bool IsPlayerComplete()
        {
            return _playerBodyParts.Count == PlayerComplete;
        }

        private bool IsPlayerDead()
        {
            return _playerBodyParts.Count == PlayerDead;
        }

        private void gameOver(bool win)
        {
            if (win)
            {
                UiManager.Instance?.YouWinScreen();
                Time.timeScale = 0f;
                fungusSystem.GetComponent<AudioSource>().Stop();
                StartCoroutine(reloadGameWithDelay(_reloadGameDelay));
            }
            else
            {
                UiManager.Instance?.GameOverScreen();
                Time.timeScale = 0f;
                fungusSystem.GetComponent<AudioSource>().Stop();
                StartCoroutine(reloadGameWithDelay(_reloadGameDelay));
            }
        }

        private IEnumerator reloadGameWithDelay(float delay)
        {
            for(float f = 0; f < delay; f += Time.unscaledDeltaTime)
            {
                if (f > _blackScreenAfter)
                {
                    _foregroundImage.color = new Color(0, 0, 0, Mathf.Lerp(0, 1, (f-_blackScreenAfter)/(_blackScreenAfter+_blackScreenDelta)));
                }
                yield return 0;
            }
            reloadGame();
        }

        private void reloadGame()
        {
            SoundManager.instance?.DisableAmbientSounds();
            SceneManager.LoadScene(0);
            Time.timeScale = 1;
        }

        #endregion
    }
}

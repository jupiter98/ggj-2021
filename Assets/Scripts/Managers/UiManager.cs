﻿using System;
using System.Collections.Generic;
using Fungus;
using TMPro;
using Tombstone_Controller;
using UI;
using UnityEngine;

namespace Managers
{
    public class UiManager : MonoBehaviour
    {
        public static UiManager Instance;

        [SerializeField] private Flowchart dialog;
        [SerializeField] private FrankyUiController bodyUIController;

        [SerializeField] private TextMeshProUGUI gameOverTitle;
        [SerializeField] private TextMeshProUGUI youWinTitle;

        #region Lifecycle

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        #endregion

        #region Public

        public void GameOverScreen()
        {
            gameOverTitle.enabled = true;
        }

        public void YouWinScreen()
        {
            youWinTitle.enabled = true;
        }

        public void ShowDialog(String block)
        {
            dialog.ExecuteBlock(block);
        }

        public void AddPiece(TombSearchable piece)
        {
            bodyUIController.AddPiece(piece);
        }

        public void RemovePiece(TombSearchable piece)
        {
            bodyUIController.RemovePiece(piece);
        }

        public void InitBodyUi(List<TombSearchable> collection)
        {
            bodyUIController.SetPieceActive(TombSearchable.Head, collection.Contains(TombSearchable.Head));
            bodyUIController.SetPieceActive(TombSearchable.Chest, collection.Contains(TombSearchable.Chest));
            bodyUIController.SetPieceActive(TombSearchable.LeftArm, collection.Contains(TombSearchable.LeftArm));
            bodyUIController.SetPieceActive(TombSearchable.RightArm, collection.Contains(TombSearchable.RightArm));
            bodyUIController.SetPieceActive(TombSearchable.LeftLeg, collection.Contains(TombSearchable.LeftLeg));
            bodyUIController.SetPieceActive(TombSearchable.RightLeg, collection.Contains(TombSearchable.RightLeg));
        }

        #endregion
    }
}
﻿using System;
using NUnit.Framework.Internal.Builders;
using Sound_Controller;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace Managers
{
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager instance;
    

        public Sound[] sounds;

        void Awake()
        {

            #region Singleton

            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }

            #endregion

            foreach (Sound s in sounds)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.clip = s.clip;
                s.source.loop = s.loop;
            }
        }


        public void EnableAmbientSounds()
        {
            instance.Play(Sound.Names.WindHowl);
            instance.Play(Sound.Names.Rain);
        }
        public void DisableAmbientSounds()
        {
            instance.StopSound(Sound.Names.WindHowl);
            instance.StopSound(Sound.Names.Rain);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                instance.Play(Sound.Names.Horse);
            }
        }

        public void Play(Sound.Names soundName)
        {
            var s = Array.Find(sounds, item => item.name == soundName); //find if there is a sound called with the name specified
            if (s == null)
            {
                Debug.LogWarning("Sound: " + name + " has not found a source to play!");
                return;
            }

            if (!s.canBeOverridden && s.source.isPlaying)
            {
                return;
            }
            
            if (s.isRandom)
            {
                s.source.volume = s.volume + Random.Range(0, s.volumeVariance);
                s.source.pitch = s.pitch + Random.Range(0, s.pitchVariance);
            }
            else
            {
                s.source.volume = s.volume;
                s.source.pitch = s.pitch;    
            }
            s.source.Play();
        }

        public void StopSound(Sound.Names soundName)
        {
            var s = Array.Find(sounds, item => item.name == soundName);
            if (s == null)
            {
                Debug.LogWarning("Sound: " + name + " has not found a source to play!");
                return;
            }

            s.source.Stop();

        }
    }
}
